local frame = {}

function frame.get_mouse_pos() -- Returns mouse position in a frame as a table
    return xluafunction_frame_get_mouse_pos()
end
function frame.get_actual_mouse_pos() -- Returns mouse position on a screen as a table
    return xluafunction_frame_get_actual_mouse_pos()
end
---@param x integer
---@param y integer
function frame.set_actual_mouse_pos(x, y) -- Sets mouse position on a screen
    xluafunction_frame_set_mouse_pos(x, y)
end
---@param size_x integer
---@param size_y integer
---@param resize_window boolean
function frame.set_frame_size(size_x, size_y, resize_window) -- Changes app's resolution
    xluafunction_frame_set_frame_size(size_x, size_y, resize_window)
end
---@param scene string
function frame.change_scene(scene) -- Changes the scene, scene being another .lua file
    xlua_frame_change_scene(scene)
end
---@param r integer
---@param g integer
---@param b integer
function frame.bg_color(r, g, b)
    xlua_frame_bg_color(r, g, b)
end

return frame