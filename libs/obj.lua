local obj = {}

obj.actions = {}
obj.conditions = {}


obj.actions.ON_CLICK = "ON_CLICK"

---@param object integer
---@param action string
---@param function_name string
function obj.add_action(object, action, function_name) -- Adds an action trigger to an object (to get action use obj.actions.[action_name])
    xluafunction_gameobj_add_action(object, action, function_name)
end

---@param object integer
function obj.conditions.MOUSE_OVER(object)
    return LuuFusion_gameobj_conditions_MOUSE_OVER(object)
end

---@param x integer
---@param y integer
---@param image_filename string
function obj.create(x, y, image_filename) -- Creates a game object and returns its fixed value, fixed value is required to modify the created object later on!
    return xlua_gameobj_create(x, y, image_filename)
end
---@param object integer
---@param x integer
---@param y integer
function obj.set_position(object, x, y) -- Sets position of the selected object
    xlua_gameobj_set_position(object, x, y)
end
---@param object integer
function obj.get_position(object) -- Returns position of the selected object as a table
    return xlua_gameobj_get_position(object)
end
---@param object integer
---@param width integer
---@param height integer
function obj.set_size(object, width, height) -- Sets width and height of the selected object
    xlua_gameobj_set_size(object, width, height)
end
---@param object integer
function obj.get_size(object) -- Returns width and height of selected object as a table
    return xlua_gameobj_get_size(object)
end
---@param object integer
---@param enabled boolean
function obj.resample(object, enabled) -- Makes object smoother when resizing, uses more CPU
    xlua_gameobj_resample(object, enabled)
end
---@param object integer
---@param degree integer
function obj.set_angle(object, degree) -- Sets angle of the selected object
    xlua_gameobj_set_angle(object, degree)
end
---@param object integer
function obj.get_angle(object) -- Returns angle of the selected object
    return xlua_gameobj_get_angle(object)
end
---@param object integer
function obj.center_hotspot(object) -- Centers the hotspot of the selected object
    xlua_gameobj_center_hotspot(object)
end
---@param object integer
---@param hotspot_x integer
---@param hotspot_y integer
function obj.set_hotspot(object, hotspot_x, hotspot_y) -- Sets custom hotspot position of the selected object
    xlua_gameobj_set_hotspot(object, hotspot_x, hotspot_y)
end
---@param object integer
---@param image_filename string
function obj.change_image(object, image_filename) -- Changes object's image
    xlua_gameobj_change_image(object, image_filename)
end
---@param object integer
function obj.exists(object) -- Checks if the selected object exists, if yes returns true
    return xlua_gameobj_exists(object)
end

return obj