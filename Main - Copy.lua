local frame = require("libs.frame")
local window = require("libs.window")
local gapi = require("libs.gapi")

local allowTyping = true
local allowTypingExec = false

function Main()

    -- Init
    frame.set_frame_size(450, 350, true)
    window.title("Terminal")
    frame.bg_color(0, 0, 0)
    window.center()

    -- Input
    Input = gapi.edit.create(0, -64, 1280, 16)

    -- Terminal
    Terminal = gapi.list.create(0, 0, 450, 350)
    gapi.list.bg_color(Terminal, 0, 0, 0)
    gapi.list.text_color(Terminal, 255, 255, 255)



    gapi.list.add_line(Terminal, "LuuOS Terminal")
    gapi.list.add_line(Terminal, "")

end

function Process()

    if allowTyping == false then
        gapi.edit.unfocus(Input)
        allowTypingExec = false
    end

    if allowTyping == true then
        if allowTypingExec == false then
            gapi.list.add_line(Terminal, "")
        end
        gapi.edit.focus(Input)
        gapi.list.change_line(Terminal, gapi.list.get_last_line_index(Terminal), "user@luuos > "..gapi.edit.get_text(Input))

        allowTypingExec = true
    end

end
