local system = {}


system.VERSION = 0.29

function system.exit() -- Exits the game
    xluafunction_system_exit()
end
function system.fps() -- Returns current fps
    return xlua_system_fps()
end
---@param max_fps_value integer
function system.set_max_fps(max_fps_value) -- Sets max frame rate of the game
    xlua_system_set_max_fps(max_fps_value)
end

return system