local console = {}

function console.show() -- Shows the console window
    xluafunction_show_console()
end
function console.hide() -- Hides the console window
    xluafunction_hide_console()
end
---@param error_text string
function console.error(error_text) -- Throws an error
    xluafunction_console_error(error_text)
end


return console