@echo off
curl -o LICENSE https://gitlab.com/shizukashiraga/LuuFusion/-/raw/main/Build/LICENSE
curl -o LuuFusion.exe https://gitlab.com/shizukashiraga/LuuFusion/-/raw/main/Build/LuuFusion.exe
mkdir libs
cd libs
curl -o console.lua https://gitlab.com/shizukashiraga/LuuFusion/-/raw/main/Build/libs/console.lua
curl -o frame.lua https://gitlab.com/shizukashiraga/LuuFusion/-/raw/main/Build/libs/frame.lua
curl -o gapi.lua https://gitlab.com/shizukashiraga/LuuFusion/-/raw/main/Build/libs/gapi.lua
curl -o keyboard.lua https://gitlab.com/shizukashiraga/LuuFusion/-/raw/main/Build/libs/keyboard.lua
curl -o obj.lua https://gitlab.com/shizukashiraga/LuuFusion/-/raw/main/Build/libs/obj.lua
curl -o system.lua https://gitlab.com/shizukashiraga/LuuFusion/-/raw/main/Build/libs/system.lua
curl -o text.lua https://gitlab.com/shizukashiraga/LuuFusion/-/raw/main/Build/libs/text.lua
curl -o window.lua https://gitlab.com/shizukashiraga/LuuFusion/-/raw/main/Build/libs/window.lua
cd ..
