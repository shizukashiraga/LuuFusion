# LuuFusion
More info on the project coming soon, still testing everything lol
# Installation / Update
Go to the folder where you want your project to be in, open terminal in that directory and then execute these:


for Linux (unstable, testing only):
```
curl -o install-linux-unstable.sh https://gitlab.com/shizukashiraga/LuuFusion/-/raw/main/InstallScripts/install-linux-unstable.sh && sh install-linux-unstable.sh && rm install-linux-unstable.sh
```
for Windows (unstable, testing only):
```
curl -o install-windows-unstable.cmd https://gitlab.com/shizukashiraga/LuuFusion/-/raw/main/InstallScripts/install-windows-unstable.cmd && install-windows-unstable.cmd && del install-windows-unstable.cmd
```
---
REMEMBER: LuuFusion starts at `Main.lua` file, you need to create it manually.

Example `Main.lua` file:
```lua
text = require("libs.text")
system = require("libs.system")
frame = require("libs.frame")

function Main() -- Executes at the start

    -- Example of creating a text object
    welcomeText = text.create(50, 50, "Welcome in LuuFusion "..system.VERSION.."!", "Arial", 24, 255, 255, 255)
    happyText = text.create(0, 0, ":3", "Arial", 20, 255, 100, 255)

end

function Process() -- Executes every frame

    -- Get mouse X and Y
    mouseX = frame.get_mouse_pos()[1]
    mouseY = frame.get_mouse_pos()[2]

    -- Move "happyText"
    text.move(happyText, mouseX+16, mouseY+16)

end
```