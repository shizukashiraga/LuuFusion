local keyboard = {}

---@param key_value integer
function keyboard.key_val_down(key_value)
    return xlua_keyboard_key_val_down(key_value)
end
---@param key_string string
function keyboard.key_down(key_string)
    return xlua_keyboard_key_down(key_string)
end
---@param key_value integer
function keyboard.key_val_pressed(key_value)
    return xlua_keyboard_key_val_pressed(key_value)
end
---@param key_string string
function keyboard.key_pressed(key_string)
    return xlua_keyboard_key_pressed(key_string)
end
---@param key_string string
function keyboard.to_value(key_string)
    return xlua_keyboard_to_value(key_string)
end
---@param key_value integer
function keyboard.to_string(key_value)
    return xlua_keyboard_to_string(key_value)
end
function keyboard.get_last_key_value()
    return xlua_keyboard_get_last_key_value()
end
function keyboard.get_last_key_string()
    return xlua_keyboard_get_last_key_string()
end

return keyboard