local window = {}

function window.get_screen_size() -- Returns the X size and Y size of the screen as a table
    return xluafunction_window_get_screen_size()
end
---@param x integer
---@param y integer
function window.move(x, y) -- Moves window to the specified position
    xluafunction_window_move(x, y)
end
function window.center()
    xlua_window_center()
end
---@param title string
function window.title(title) -- Sets window's title
    xlua_window_title(title)
end

return window