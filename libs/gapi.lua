-- gapi: Allows you to use Windows's/Wine's/LuuOS's native system objects in your application
-- like buttons, menus, lists...

local gapi = {}

gapi.button = {}
gapi.button.actions = {}
gapi.edit = {}
gapi.list = {}

------------------------------------------------------------ button ------------------------------------------------------------
gapi.button.actions.ON_CLICK = "ON_CLICK"

---@param button integer
---@param action string
---@param function_name string
function gapi.button.add_action(button, action, function_name)
    xlua_winapi_button_add_action(button, action, function_name)
end
---@param x integer
---@param y integer
---@param size_x integer
---@param size_y integer
---@param text string
function gapi.button.create(x, y, size_x, size_y, text)
    return xlua_winapi_button_create(x, y, size_x, size_y, text)
end
------------------------------------------------------------ edit ------------------------------------------------------------
---@param x integer
---@param y integer
---@param height integer
---@param width integer
function gapi.edit.create(x, y, height, width)
    return xlua_os_edit_create(x, y, height, width)
end
function gapi.edit.get_text(obj)
    return xlua_os_edit_get_text(obj)
end
---@param text string
function gapi.edit.set_text(obj, text)
    xlua_os_edit_set_text(obj, text)
end
function gapi.edit.focus(obj)
    xlua_gapi_edit_focus(obj)
end
function gapi.edit.unfocus(obj)
    xlua_gapi_edit_unfocus(obj)
end
------------------------------------------------------------ list ------------------------------------------------------------
---@param x integer
---@param y integer
---@param height integer
---@param width integer
function gapi.list.create(x, y, height, width)
    return xlua_os_list_create(x, y, height, width)
end
---@param obj integer
---@param r integer
---@param g integer
---@param b integer
function gapi.list.bg_color(obj, r, g, b)
    xlua_os_list_bg_color(obj, r, g, b)
end
---@param obj integer
---@param r integer
---@param g integer
---@param b integer
function gapi.list.text_color(obj, r, g, b)
    xlua_gapi_list_text_color(obj, r, g, b)
end
---@param text string
function gapi.list.add_line(obj, text)
    xlua_gapi_list_add_line(obj, text)
end
---@param obj any
---@param line_index integer
---@param text string
function gapi.list.change_line(obj, line_index, text)
    xlua_gapi_list_change_line(obj, line_index, text)
end
---@param obj any
function gapi.list.get_last_line_index(obj)
    return xlua_gapi_list_get_last_line_index(obj)
end

return gapi