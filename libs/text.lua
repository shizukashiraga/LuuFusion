local text = {}

---@param x integer
---@param y integer
---@param text string
---@param font_name string
---@param font_size integer
---@param r integer
---@param g integer
---@param b integer
function text.create(x, y, text, font_name, font_size, r, g, b) -- Creates a text object and returns its fixed value, fixed value is required to modify the created object later on!
    return xluafunction_text_create(x, y, text, font_name, font_size, r, g, b)
end
---@param object integer
---@param x integer
---@param y integer
function text.move(object, x, y) -- Changes selected text object position
    xluafunction_text_move(object, x, y)
end
---@param object integer
---@param text string
function text.modify(object, text) -- Modifies the text of the text object
    xluafunction_text_modify(object, text)
end
---@param object integer
---@param font_name string
function text.set_font(object, font_name) -- Changes the font of the selected text object
    xluafunction_text_set_font(object, font_name)
end
---@param object integer
---@param font_size integer
function text.set_size(object, font_size) -- Sets size of the selected text object
    xluafunction_text_set_size(object, font_size)
end
---@param object integer
---@param r integer
---@param g integer
---@param b integer
function text.change_color(object, r, g, b) -- Changes selected text object color
    xluafunction_text_change_color(object, r, g, b)
end
---@param object integer
---@param bold boolean
function text.bold(object, bold) -- Makes text object bold (doesn't work for some reason lol, atleast on my machine)
    xlua_text_bold(object, bold)
end
---@param object integer
function text.get(id) -- Returns text of the selected text object
    return xlua_text_get(id)
end

return text